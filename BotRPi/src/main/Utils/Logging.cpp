#include "Utils/Logging.hpp"

using namespace Utils;

Logging::Logging() {
    log.open("log.txt");
    std::cout << "A Logging object has been created" << std::endl;
}

Logging::~Logging() {
    log.close();
    std::cout << "A Logging object has been destroyed" << std::endl;
}

int Logging::write(int level, std::string msg) {
    if(!log) {
        std::cout << "Error opening file for output" << std::endl;
        return -1;
    }
    std::string temp;
    switch(level) {
        case 0:
            temp = temp + "INFO";
            break;
        case 1:
            temp = temp + "WARN";
            break;
        case 2:
            temp = temp + "ERROR";
            break;
        default:
            temp = temp + "UNDEFINED_LEVEL_" + std::to_string(level);
            break;
    }
    temp = temp + ": " + msg;
    log << temp << std::endl;
    if(level >= logLevel) {
        std::cout << temp << std::endl;
    }
    temp.clear(); // Doesn't this variable already get destroyed once the scope is left? not sure lol
    return 0;
}

int Logging::setLogLevel(int level) {
    this -> logLevel = level;
    return getLogLevel();
}

int Logging::getLogLevel() {
    return this -> logLevel;
}
