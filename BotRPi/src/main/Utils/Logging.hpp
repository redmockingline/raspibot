#ifndef iostream_H
#define iostream_H
#include <iostream>
#endif

#ifndef fstream_H
#define fstream_H
#include <fstream>
#endif

#ifndef string_H
#define string_H
#include <string>
#endif

#ifndef Logging_H
#define Logging_H

namespace Utils {
    class Logging {
    public:
        Logging();
        ~Logging();
        int write(int, std::string);
        int setLogLevel(int);
        int getLogLevel();

    private:
        int logLevel = 2;
        std::ofstream log;
    };
}

#endif
