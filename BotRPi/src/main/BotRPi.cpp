#ifndef iostream_H
#define iostream_H
#include <iostream>
#endif

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>


//#include "Utils/Logging.hpp"

//#include "Comms.hpp" im not using this yet

//#include <pigpio.h> // for controlling GPIO pins on raspberry pi
//#include <pthread.h> // use Boost.Thread if this is not available on ARM64 (RPi)

namespace logging = boost::log;

void init(){
    logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::warning);
}

int main(){
    std::cout << "### Raspberry Pi Robot Program Started! ###" << std::endl;

    init();

    BOOST_LOG_TRIVIAL(trace) << "A trace severity message";
    BOOST_LOG_TRIVIAL(debug) << "A debug severity message";
    BOOST_LOG_TRIVIAL(info) << "An informational severity message";
    BOOST_LOG_TRIVIAL(warning) << "A warning severity message";
    BOOST_LOG_TRIVIAL(error) << "An error severity message";
    BOOST_LOG_TRIVIAL(fatal) << "A fatal severity message";

/*
    Utils::Logging log;
    log.write(0, "info Test");
    log.write(2, "error test");
    log.setLogLevel(2);
    log.write(0, "info Test after");
    log.write(2, "error test after");
*/
    return 0;
}
