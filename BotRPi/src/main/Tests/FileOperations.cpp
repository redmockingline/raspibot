#include "Tests/FileOperations.hpp"

using namespace Tests;

int FileOperations::testWrite() {
    std::ofstream writer("FileOperationsTest.txt");
    if(!writer) {
        std::cout << "Error opening file for output" << std::endl;
        return -1;
    }
    writer << "Testing\nOutput" << std::endl;
    writer.close();
    return 0;
}
