# Raspberry PI

This is the code that will be deployed to a Raspberry PI. This is the master program for robot operation, that will take input from a driver station and control the arduino slaves

</br>

## Development Environment - Updated Oct 8, 2020
### Dependancies
- pigpio

### Linux (possibly out of date)
- Distribution: Debian 10 x86-64
- C++ Compiler: g++ 8.3.0
- C++ Debugger: gdb 8.2.1
- C++ Version: C++17
- Version Control System: git 2.20.1
- Build/Deployment System: Gradle 6.1
- IDE: VSCode (or VSCodium) with specified configuration

</br>

Links:
</br>
[Gradle C++ Application Plugin](https://docs.gradle.org/6.3/userguide/cpp_application_plugin.html)
</br>
[Gradle C++ Library Plugin](https://docs.gradle.org/6.3/userguide/cpp_library_plugin.html)
</br>
[Gradle C++ Unit Test Plugin](https://docs.gradle.org/current/userguide/cpp_unit_test_plugin.html)
</br>
[Writing Gradle Tasks](https://guides.gradle.org/writing-gradle-tasks/)
</br>
[Debugging C++ in VSCode](https://code.visualstudio.com/docs/cpp/cpp-debug)

</br>

This code sticks with the project template from [here](https://gitlab.com/templates18/cpp/vscode-gradle-application)
