# Raspibot

## PLEASE READ:

This is the source directory for developing my robot project. This features a Raspberry Pi on the robot utilizing an arduino as a slave for controlling and reading hardware, while allowing for teleoperation, advanced processing, and various other forms advanced functionality.

DISCLAIMER:
This project is in its very early stages of development.
This project may violate licensing in its current form.
This project WILL NOT WORK.
Once the project becomes functional, I will review all licensing issues and resolve them. I will also create documentation for the project and update it as I expand the project.
After the documentation is created and there are no licensing issues that I know about (I am a hobbyist, not a lawyer), I will release the code to the public.

If you are reading this now, I warmly welcome input on this project on how to better develop it and protect myself and others, particularly in the legal realm. This project will be developed to comply with any US regulations. Configuration for other countries will be added once I near the end of my plans for this project (notably for the planned radio tele-operation feature)

# Start of README

Dependencies:
- Boost 1.75.0 [Download](https://www.boost.org/users/download/)


Arduino1.tar and DriverStation.tar exist because I layed out the framework to start those projects but I am not developing them yet. I added them into archives to clean up the tree and keep them out of the way. They will be extracted and updated when I am ready to begin development.
