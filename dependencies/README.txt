This directory contains any files that the projects may depend on.

It is a central place for them that way there isn't as much of a dependancy on the system configuration.
